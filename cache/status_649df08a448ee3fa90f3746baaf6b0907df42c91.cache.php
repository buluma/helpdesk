<?php if (!defined('IN_SCRIPT')) {die();} $hesk_settings['statuses']=array (
  0 => 
  array (
    'name' => 'New',
    'class' => 'open',
  ),
  1 => 
  array (
    'name' => 'Waiting reply',
    'class' => 'waitingreply',
  ),
  2 => 
  array (
    'name' => 'Replied',
    'class' => 'replied',
  ),
  3 => 
  array (
    'name' => 'Resolved',
    'class' => 'resolved',
  ),
  4 => 
  array (
    'name' => 'In Progress',
    'class' => 'inprogress',
  ),
  5 => 
  array (
    'name' => 'On Hold',
    'class' => 'onhold',
  ),
  6 => 
  array (
    'name' => 'Quote Sent',
    'color' => '#5447ff',
    'can_customers_change' => '0',
  ),
  7 => 
  array (
    'name' => 'Vehicle Booked',
    'color' => '#10c74e',
    'can_customers_change' => '0',
  ),
);