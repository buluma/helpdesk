<?php
/**
 *
 * This file is part of HESK - PHP Help Desk Software.
 *
 * (c) Copyright Klemen Stirn. All rights reserved.
 * https://www.hesk.com
 *
 * For the full copyright and license agreement information visit
 * https://www.hesk.com/eula.php
 *
 */

define('IN_SCRIPT',1);
define('HESK_PATH','../');

/* Get all the required files and functions */
require(HESK_PATH . 'hesk_settings.inc.php');
require(HESK_PATH . 'inc/common.inc.php');
require(HESK_PATH . 'inc/admin_functions.inc.php');
hesk_load_database_functions();

hesk_session_start();
hesk_dbConnect();
hesk_isLoggedIn();

/* Check permissions for this feature */
hesk_checkPermission('can_man_branch');

// Possible priorities
$priorities = array(
	3 => array('value' => 3, 'text' => $hesklang['low'],		'formatted' => $hesklang['low']),
	2 => array('value' => 2, 'text' => $hesklang['medium'],		'formatted' => '<font class="medium">'.$hesklang['medium'].'</font>'),
	1 => array('value' => 1, 'text' => $hesklang['high'],		'formatted' => '<font class="important">'.$hesklang['high'].'</font>'),
	0 => array('value' => 0, 'text' => $hesklang['critical'],	'formatted' => '<font class="critical">'.$hesklang['critical'].'</font>'),
);

/* What should we do? */
if ( $action = hesk_REQUEST('a') )
{
	if ($action == 'linkcode')       {generate_link_code();}
	elseif ( defined('HESK_DEMO') )  {hesk_process_messages($hesklang['ddemo'], 'manage_branches.php', 'NOTICE');}
	elseif ($action == 'new')        {new_branch();}
	elseif ($action == 'rename')     {rename_branch();}
	elseif ($action == 'remove')     {remove();}
	elseif ($action == 'order')      {order_branch();}
	elseif ($action == 'autoassign') {toggle_autoassign();}
	elseif ($action == 'type')       {toggle_type();}
	elseif ($action == 'priority')   {change_priority();}
}

/* Print header */
require_once(HESK_PATH . 'inc/header.inc.php');

/* Print main manage users page */
require_once(HESK_PATH . 'inc/show_admin_nav.inc.php');
?>

</td>
</tr>
<tr>
<td>

<script language="Javascript" type="text/javascript"><!--
function confirm_delete()
{
if (confirm('<?php echo hesk_makeJsString($hesklang['confirm_del_branch']); ?>')) {return true;}
else {return false;}
}
//-->
</script>

<?php
/* This will handle error, success and notice messages */
hesk_handle_messages();
?>

<!-- <h3 style="padding-bottom:5px"><?php //echo $hesklang['manage_branch']; ?> [<a href="javascript:void(0)" onclick="javascript:alert('<?php //echo hesk_makeJsString($hesklang['branch_intro']); ?>')">?</a>]</h3> -->

<h3 style="padding-bottom:5px">User Login Logs</h3>
<p>Showing User logins for the last 5 days</p>

<!-- Buluma -->
<?php
/*Get today's logins*/

// $today_logins = hesk_dbQuery('SELECT * FROM `'.hesk_dbEscape($hesk_settings['db_pfix']).'user_logins` WHERE `login_time` >= now() - INTERVAL 3 DAY ORDER BY `id` DESC');

$today_logins = hesk_dbQuery('SELECT * FROM `'.hesk_dbEscape($hesk_settings['db_pfix']).'user_logins` WHERE `login_time` >= DATE_SUB(CURDATE(), INTERVAL 5 DAY) AND login_time <= CURDATE()
ORDER BY login_time DESC');
// var_dump($today_logins);
$num_logins = hesk_dbNumRows($today_logins);
?>

<div align="center" class="">

<table border="0" cellspacing="1" cellpadding="3" class="white" width="100%">
    <tr>
			<th>ID</th>
			<th>User</th>
			<th>Login Time</th>
			<th>IP Address</th>
			<th>Security ID</th>
    </tr>
    <?php
    $i = 0;
    foreach ($today_logins as $r) {
        echo "<tr>";
        echo "<td class='admin_gray'>" . $r['id'] . "</td><td class='admin_white'>" . strtolower(trim(($r['user']))) . "</td><td class='admin_white'>" . strtolower(trim($r['login_time'])) . "</td><td class='admin_white'>" . strtolower(trim($r['ip_address'])) . "</td><td class='admin_gray'>" . strtolower(trim($r['sec_id'])) . "</td>";
        echo "</tr>";

        $i++;
    }
    ?>
</table>

	</div>
<!-- End Buluma -->


<!-- HR -->
<p>&nbsp;</p>

<?php
require_once(HESK_PATH . 'inc/footer.inc.php');
exit();


/*** START FUNCTIONS ***/

function change_priority()
{
	global $hesk_settings, $hesklang, $priorities;

	/* A security check */
	hesk_token_check('POST');

	$_SERVER['PHP_SELF'] = 'manage_branches.php?branchid='.intval( hesk_POST('branchid') );

	$branchid = hesk_isNumber( hesk_POST('branchid'), $hesklang['choose_branch_ren'], $_SERVER['PHP_SELF']);
	$_SESSION['selbranch'] = $branchid;
	$_SESSION['selbranch2'] = $branchid;

	$priority = intval( hesk_POST('priority', 3));
	if ( ! array_key_exists($priority, $priorities) )
	{
		$priority = 3;
	}

	hesk_dbQuery("UPDATE `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` SET `priority`='{$priority}' WHERE `id`='".intval($branchid)."'");

    hesk_cleanSessionVars('branch_ch_priority');

	hesk_process_messages($hesklang['branch_pri_ch'].' '.$priorities[$priority]['formatted'],$_SERVER['PHP_SELF'],'SUCCESS');
} // END change_priority()


function generate_link_code() {
	global $hesk_settings, $hesklang;
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML; 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
	<title><?php echo $hesklang['genl']; ?></title>
	<meta http-equiv="Content-Type" content="text/html;charset=<?php echo $hesklang['ENCODING']; ?>" />
	<style type="text/css">
		body{ margin:5px 5px; padding:0; background:#fff; color: black; font : 68.8%/1.5 Verdana, Geneva, Arial, Helvetica, sans-serif;}
		p{ color : black; font-family : Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 1.0em;}
		h3 { color : #AF0000; font-family : Verdana, Geneva, Arial, Helvetica, sans-serif; font-weight: bold; font-size: 1.0em;}
	</style>
	</head>
	<body>

	<div style="text-align:center">
		<h3><?php echo $hesklang['genl']; ?></h3>

		<?php
		if ( ! empty($_GET['p']) )
		{
			echo '<p>&nbsp;<br />' . $hesklang['cpric'] . '<br />&nbsp;</p>';
		}
		else
		{
			?>
			<p><i><?php echo $hesklang['genl2']; ?></i></p>

			<textarea rows="3" cols="50" onfocus="this.select()"><?php echo $hesk_settings['hesk_url'].'/index.php?a=add&amp;branchid='.intval( hesk_GET('branchid') ); ?></textarea>
			<?php
		}
		?>

		<p align="center"><a href="#" onclick="Javascript:window.close()"><?php echo $hesklang['cwin']; ?></a></p>
	</div>
	</body>
	</html>
	<?php
	    exit();
}


function new_branch()
{
	global $hesk_settings, $hesklang;

	/* A security check */
	hesk_token_check('POST');

    /* Options */
    $_SESSION['branch_autoassign'] = hesk_POST('autoassign') == 'Y' ? 1 : 0;
    $_SESSION['branch_type'] = hesk_POST('type') == 'Y' ? 1 : 0;

	// Default priority
	$_SESSION['branch_priority'] = intval( hesk_POST('priority', 3) );
	if ($_SESSION['branch_priority'] < 0 || $_SESSION['branch_priority'] > 3)
	{
		$_SESSION['branch_priority'] = 3;
	}

    /* Category name */
	$branchname = hesk_input( hesk_POST('name') , $hesklang['enter_branch_name'], 'manage_branches.php');

    /* Do we already have a branch with this name? */
	$res = hesk_dbQuery("SELECT `id` FROM `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` WHERE `name` LIKE '".hesk_dbEscape( hesk_dbLike($branchname) )."' LIMIT 1");
    if (hesk_dbNumRows($res) != 0)
    {
		$_SESSION['branchname'] = $branchname;
		hesk_process_messages($hesklang['cndupl'],'manage_branches.php');
    }

	/* Get the latest branch_order */
	$res = hesk_dbQuery("SELECT `branch_order` FROM `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` ORDER BY `branch_order` DESC LIMIT 1");
	$row = hesk_dbFetchRow($res);
	$my_order = $row[0]+10;

	hesk_dbQuery("INSERT INTO `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` (`name`,`branch_order`,`autoassign`,`type`, `priority`) VALUES ('".hesk_dbEscape($branchname)."','".intval($my_order)."','".intval($_SESSION['branch_autoassign'])."','".intval($_SESSION['branch_type'])."','{$_SESSION['branch_priority']}')");

    hesk_cleanSessionVars('branchname');
    hesk_cleanSessionVars('branch_autoassign');
    hesk_cleanSessionVars('branch_type');
    hesk_cleanSessionVars('branch_priority');

    $_SESSION['selbranch2'] = hesk_dbInsertID();

	hesk_process_messages(sprintf($hesklang['branch_name_added'],'<i>'.stripslashes($branchname).'</i>'),'manage_branches.php','SUCCESS');
} // End new_branch()


function rename_branch()
{
	global $hesk_settings, $hesklang;

	/* A security check */
	hesk_token_check('POST');

    $_SERVER['PHP_SELF'] = 'manage_branches.php?branchid='.intval( hesk_POST('branchid') );

	$branchid = hesk_isNumber( hesk_POST('branchid'), $hesklang['choose_branch_ren'], $_SERVER['PHP_SELF']);
	$_SESSION['selbranch'] = $branchid;
    $_SESSION['selbranch2'] = $branchid;

	$branchname = hesk_input( hesk_POST('name'), $hesklang['branch_ren_name'], $_SERVER['PHP_SELF']);
    $_SESSION['branchname2'] = $branchname;

	$res = hesk_dbQuery("SELECT `id` FROM `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` WHERE `name` LIKE '".hesk_dbEscape( hesk_dbLike($branchname) )."' LIMIT 1");
    if (hesk_dbNumRows($res) != 0)
    {
    	$old = hesk_dbFetchAssoc($res);
        if ($old['id'] == $branchid)
        {
        	hesk_process_messages($hesklang['noch'],$_SERVER['PHP_SELF'],'NOTICE');
        }
        else
        {
    		hesk_process_messages($hesklang['cndupl'],$_SERVER['PHP_SELF']);
        }
    }

	hesk_dbQuery("UPDATE `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` SET `name`='".hesk_dbEscape($branchname)."' WHERE `id`='".intval($branchid)."'");

    unset($_SESSION['selbranch']);
    unset($_SESSION['branchname2']);

    hesk_process_messages($hesklang['branch_renamed_to'].' <i>'.stripslashes($branchname).'</i>',$_SERVER['PHP_SELF'],'SUCCESS');
} // End rename_branch()


function remove()
{
	global $hesk_settings, $hesklang;

	/* A security check */
	hesk_token_check();

    $_SERVER['PHP_SELF'] = 'manage_branches.php';

	$mybranch = intval( hesk_GET('branchid') ) or hesk_error($hesklang['no_branch_id']);
	if ($mybranch == 1)
    {
    	hesk_process_messages($hesklang['cant_del_default_branch'],$_SERVER['PHP_SELF']);
    }

	hesk_dbQuery("DELETE FROM `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` WHERE `id`='".intval($mybranch)."'");
	if (hesk_dbAffectedRows() != 1)
    {
    	hesk_error("$hesklang[int_error]: $hesklang[branch_not_found].");
    }

	hesk_dbQuery("UPDATE `".hesk_dbEscape($hesk_settings['db_pfix'])."tickets` SET `branch`=1 WHERE `branch`='".intval($mybranch)."'");

    hesk_process_messages($hesklang['branch_removed_db'],$_SERVER['PHP_SELF'],'SUCCESS');
} // End remove()


function order_branch()
{
	global $hesk_settings, $hesklang;

	/* A security check */
	hesk_token_check();

	$branchid = intval( hesk_GET('branchid') ) or hesk_error($hesklang['branch_move_id']);
	$_SESSION['selbranch2'] = $branchid;

	$branch_move=intval( hesk_GET('move') );

	hesk_dbQuery("UPDATE `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` SET `branch_order`=`branch_order`+".intval($branch_move)." WHERE `id`='".intval($branchid)."'");
	if (hesk_dbAffectedRows() != 1)
    {
    	hesk_error("$hesklang[int_error]: $hesklang[branch_not_found].");
    }

	/* Update all branch fields with new order */
	$res = hesk_dbQuery("SELECT `id` FROM `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` ORDER BY `branch_order` ASC");

	$i = 10;
	while ($mybranch=hesk_dbFetchAssoc($res))
	{
	    hesk_dbQuery("UPDATE `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` SET `branch_order`=".intval($i)." WHERE `id`='".intval($mybranch['id'])."'");
	    $i += 10;
	}

    header('Location: manage_branches.php');
    exit();
} // End order_branch()


function toggle_autoassign()
{
	global $hesk_settings, $hesklang;

	/* A security check */
	hesk_token_check();

	$branchid = intval( hesk_GET('branchid') ) or hesk_error($hesklang['branch_move_id']);
	$_SESSION['selbranch2'] = $branchid;

    if ( intval( hesk_GET('s') ) )
    {
		$autoassign = 1;
        $tmp = $hesklang['caaon'];
    }
    else
    {
        $autoassign = 0;
        $tmp = $hesklang['caaoff'];
    }

	/* Update auto-assign settings */
	$res = hesk_dbQuery("UPDATE `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` SET `autoassign`='".intval($autoassign)."' WHERE `id`='".intval($branchid)."'");
	if (hesk_dbAffectedRows() != 1)
    {
        hesk_process_messages($hesklang['int_error'].': '.$hesklang['branch_not_found'],'./manage_branches.php');
    }

    hesk_process_messages($tmp,'./manage_branches.php','SUCCESS');

} // End toggle_autoassign()


function toggle_type()
{
	global $hesk_settings, $hesklang;

	/* A security check */
	hesk_token_check();

	$branchid = intval( hesk_GET('branchid') ) or hesk_error($hesklang['branch_move_id']);
	$_SESSION['selbranch2'] = $branchid;

    if ( intval( hesk_GET('s') ) )
    {
		$type = 1;
        $tmp = $hesklang['cpriv'];
    }
    else
    {
        $type = 0;
        $tmp = $hesklang['cpub'];
    }

	/* Update auto-assign settings */
	hesk_dbQuery("UPDATE `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` SET `type`='{$type}' WHERE `id`='".intval($branchid)."'");
	if (hesk_dbAffectedRows() != 1)
    {
        hesk_process_messages($hesklang['int_error'].': '.$hesklang['branch_not_found'],'./manage_branches.php');
    }

    hesk_process_messages($tmp,'./manage_branches.php','SUCCESS');

} // End toggle_type()
?>
