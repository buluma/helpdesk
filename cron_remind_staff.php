<?php
define('IN_SCRIPT',1);
define('HESK_PATH','/Applications/MAMP/htdocs/helpdesk/');

// Get all the required files and functions
require(HESK_PATH . 'hesk_settings.inc.php');
require(HESK_PATH . 'inc/common.inc.php');

hesk_load_database_functions();
require(HESK_PATH . 'inc/email_functions.inc.php');
require(HESK_PATH . 'inc/posting_functions.inc.php');

function get_unreplied_tickets(){
    global $hesk_settings, $hesklang, $ticket;
    // get unreplied tickets
    $columns = 't.`id`, t.`trackid`, t.`name`, t.`email`, t.`dt` as created_on, t.`staffreplies`, t.`owner`, t.`closedat`, 
    u.`id` as userid, u.`user` as username, u.`name` as staffname, u.`email` as staffemail';
    //$res = hesk_dbQuery("SELECT ".$columns." FROM `hesk_tickets` t LEFT JOIN `hesk_users` u on t.`owner` = u.`id` WHERE t.`staffreplies` = '0' AND t.`closedat` IS NULL");
    $res = hesk_dbQuery("SELECT * FROM `hesk_tickets` WHERE `staffreplies` = '0' AND `closedat` IS NULL");
    
    $tickets = array();

    while ($tct = hesk_dbFetchAssoc($res))
    {
      $tickets[] = $tct; 
    }
    // echo '<pre>';
    // print_r($tickets);
    // echo '</pre>';
    $now = Date('Y-m-d H:i:s');
    //echo $now;
    //global $hesk_settings, $hesklang, $ticket;

    foreach ($tickets as $ticket) {
      global $ticket;
      $datenow = strtotime($now);
      $created_on = strtotime($ticket['dt']);
      $elapsed = $datenow - $created_on;
      $fourhours = 4 * 3600; // get the number of seconds in 4 hours = 14400
      $hourselapsed = $elapsed / 3600;

      //echo $elapsed . ' - '.$hourselapsed. ' - '. $fourhours . ' ;; ';
      // if elapsed time is greater than 3 hrs 50min (11400) notify staff again
      $threefifty = 11400;

      if ($elapsed > $threefifty && $elapsed < $fourhours || $elapsed >= $fourhours){
        echo $ticket['trackid']. ' sending email to staff '. $elapsed.'<br>';
        // hesk_notifyAssignedStaff(false, 'ticket_assigned_to_you_reminder');
      }
      else {
        echo $ticket['trackid']. ' 4 hours not yet near '. $elapsed.'<br>';
      }
    }    
}

get_unreplied_tickets();